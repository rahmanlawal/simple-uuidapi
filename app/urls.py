from . import views
from django.urls import path

urlpatterns = [
    path(
        "", view=views.UUIDView.as_view()
    ),
]