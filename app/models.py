from django.db import models
from django.utils import timezone


# Create your models here.

class AllUUIDS(models.Model):
    uuid_str = models.UUIDField()
    created_at = models.DateTimeField(default=timezone.now)