import uuid
from . import models
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

class UUIDView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, *args, **kwargs):
        models.AllUUIDS.objects.create(
            uuid_str= uuid.uuid1(),
        )
        data = [{f"{obj.created_at}": f"{obj.uuid_str}"} for obj in models.AllUUIDS.objects.all().order_by('-created_at')]
        return Response(data=data, status=status.HTTP_200_OK)